import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class FieldVenue extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public venueId: number

  @column()
  public fieldId: number

}
