import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, validator, rules } from '@ioc:Adonis/Core/Validator'
// import Database from '@ioc:Adonis/Lucid/Database'
import User from 'App/Models/User'


enum role {
    USER = 'user', 
    OWNER = 'owner'
}

export default class AuthController {
    /**
  * @swagger
  * /user/login:
  *   post:
  *     tags:
  *       - User
  *     summary: Sample API
  *     parameters:
  *       - name: email
  *         description: Email of the user
  *         in: body
  *         required: false
  *         type: string
  *       - name: password
  *         description: Password of the user
  *         in: body
  *         required: false
  *         type: string
  *     produces:
  *      - application/json
  *     responses:
  *       200:
  *         type: bearer
  *         token: token example
  *         expires_at: expire token
  */
  public async login ({ request, auth }: HttpContextContract) {
    const email = request.input('email')
    const password = request.input('password')

    const token = await auth.use('api').attempt(email, password, {
        expiresIn: '2 days',
    })
    return token.toJSON()
  }

  public async logout ({ response, auth }: HttpContextContract) {
    await auth.use('api').logout()

    return response.status(200).json({message : "Logout success"})
  }

  public async createUser({request, response} : HttpContextContract){
        const postSchema = schema.create({      
            name: schema.string(),
            role : schema.enum(Object.values(role)),
            password : schema.string(),
            email : schema.string({}, [
                rules.email()
            ])
        })

        const data = await request.validate({      
            schema: postSchema,      
            cacheKey: request.url(),    
        })

        let user = new User();
        user.name = data.name
        user.password = data.password
        user.email = data.email
        user.role = data.role

        await user.save();
        return response.status(200).json({message: "Create Success", data: user.serialize()});
    }    
}
