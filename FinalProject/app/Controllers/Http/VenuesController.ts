import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, validator, rules } from '@ioc:Adonis/Core/Validator'
// import Database from '@ioc:Adonis/Lucid/Database'
import Venue from 'App/Models/Venue'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field';
import User from 'App/Models/User';

export default class VenuesController {
    public async getAll({response, auth} : HttpContextContract){
        // const listVenues = await Database.query().select(['id', 'name', 'address','phone']).from('venues')
        const user = auth.user;
        if(user?.role!='owner'){
            return response.status(401).json({message: "Unauthorized"});
        }

        const listVenues = await Venue.query().preload('field')
        return response.status(200).json({message: "Get data success", data:listVenues});
    }

    public async getById({params, response, auth} : HttpContextContract){
        // const venues = await Database.query().select(['id', 'name', 'address','phone'])
        //     .from('venues').where('id', params.id).first()
        const user = auth.user;
        if(user?.role!='owner'){
            return response.status(401).json({message: "Unauthorized"});
        }

        let venues = await Venue.query()
                        .where('id', params.id)
                        .preload('field')
        
        return response.status(200).json({message: "Get data success", data:venues});
    }

    public async deleteById({params, response, auth} : HttpContextContract){
        // await Database.from('venues').where('id', params.id).delete()
        const user = auth.user;
        if(user?.role!='owner'){
            return response.status(401).json({message: "Unauthorized"});
        }

        const venues = await Venue.findBy('id', params.id)
        await venues?.delete()
        return response.status(200).json({message: "Delete success"});
    }

    public async create({request, response, auth} : HttpContextContract){
        const user = auth.user;
        if(user?.role!='owner'){
            return response.status(401).json({message: "Unauthorized"});
        }

        let data = request.only(['name', 'address', 'phone', 'field'])
        let venue = new Venue()
        venue.name = data.name
        venue.address = data.address
        venue.phone = data.phone

        let venueSave = await venue.save();
        let fieldData = new Field();
        fieldData.name = data.field.name;
        fieldData.type = data.field.type;
        fieldData.id = venueSave.id;
        
        venue.related('field').save(fieldData, false)

        return response.status(200).json({message: "Create Success", data: venue.serialize()});
    }

    public async updateById({params, request,  response, auth} : HttpContextContract){
        // const venues = await Database.query().select('*')
        //     .from('venues').where('id', params.id).first()
        const user = auth.user;
        if(user?.role!='owner'){
            return response.status(401).json({message: "Unauthorized"});
        }

        let venues = await Venue.findBy('id', params.id)
        venues = venues==null ? new Venue() : venues
        
        let saveUpdate = request.all();
        if(saveUpdate.name != undefined && saveUpdate.name != ''){
            venues.name = saveUpdate.name
        }

        if(saveUpdate.address != undefined && saveUpdate.address != ''){
            venues.address = saveUpdate.address
        }

        if(saveUpdate.phone != undefined && saveUpdate.phone != ''){
            venues.phone = saveUpdate.phone
        }
        await venues.save()
        // await Database
        //     .from('venues')
        //     .where('id', params.id)
        //     .update(saveUpdate)


        return response.status(200).json({message: "Update success", data: venues.serialize()});
    }

    public async validation({request, response, auth}: HttpContextContract){
        const user = auth.user;
        if(user?.role!='owner'){
            return response.status(401).json({message: "Unauthorized"});
        }

        const postSchema = schema.create({      
            nama : schema.string(),      
            alamat : schema.string(),   
            phone: schema.string({}, [
                rules.mobile({ strict: true })
            ])
        })
        const data = await request.validate({      
            schema: postSchema,      
            cacheKey: request.url(),    
        })

        response.status(200).json({message: 'Data is valid', data})
    }

    public async booking({request, response, auth}: HttpContextContract){
        let user = auth.user;
        if(user?.role!='user'){
            return response.status(401).json({message: "Unauthorized"});
        }
        const postSchema = schema.create({      
            field_id : schema.number(),   
            play_date_start: schema.date({}, [
                rules.after('today')
            ]),
            play_date_end: schema.date({}, [
                rules.afterField('play_date_start')
            ])
        })
        const data = await request.validate({      
            schema: postSchema,      
            cacheKey: request.url(),    
        })

        let booking = new Booking()
        booking.fieldId = data.field_id
        booking.playDateStart = data.play_date_start
        booking.playDateEnd = data.play_date_end
        booking.userId = user == undefined ? 0 : user.id
        await booking.save()

        user = user == undefined ? new User() : user;
        await booking.related('users').save(user)

        response.status(200).json({message: 'Booking success'})
    }

    public async join({request, response, auth, params}: HttpContextContract){
        let user = auth.user
            
        let bookingData = await Booking.query().preload('users').where("id", params.booking_id).first()
        bookingData = bookingData==undefined ? new Booking() : bookingData;
        user = user == undefined ? new User() : user;
        
        bookingData.related('users').sync([user.id]);

        response.status(200).json({message: 'Join success'})
    }
}
