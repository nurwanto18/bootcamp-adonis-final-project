import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema, validator, rules } from '@ioc:Adonis/Core/Validator'
// import Database from '@ioc:Adonis/Lucid/Database'
import Field from 'App/Models/Field'

enum type {
    FUTSAL = 'futsal', 
    MINI_SOCCER = 'mini soccer', 
    BASKETBALL = 'basketball'
}

export default class FieldsController {
    public async getAll({response} : HttpContextContract){
        // const listFields = await Database.query().select(['id', 'name', 'type', 'venue_id']).from('fields')
        const listFields = await Field.all()
        return response.status(200).json({message: "Get data success", listFields});
    }

    public async getById({params, response} : HttpContextContract){
        // const fields = await Database.query().select(['id', 'name', 'type', 'venue_id'])
        //     .from('fields').where('id', params.id).first()
        let fields = await Field.findBy('id', params.id)
        let data = fields?.serialize()
        return response.status(200).json({message: "Get data success", data});
    }

    public async deleteById({params, response} : HttpContextContract){
        // await Database.from('fields').where('id', params.id).delete()
        const fields = await Field.findBy('id', params.id)
        await fields?.delete()
        return response.status(200).json({message: "Delete success"});
    }

    public async create({request, response} : HttpContextContract){
        const postSchema = schema.create({      
            name: schema.string(),
            type : schema.enum(Object.values(type)),
            venue_id : schema.number()
        })

        const data = await request.validate({      
            schema: postSchema,      
            cacheKey: request.url(),    
        })

        // await Database
        //     .insertQuery() // 👈 gives an instance of insert query builder
        //     .table('fields')
        //     .insert(data)
        let field = new Field()
        field.name = data.name
        field.type = data.type
        field.venue_id = data.venue_id

        await field.save();
        return response.status(200).json({message: "Create Success", data: field.serialize()});
    }

    public async updateById({params, request, response} : HttpContextContract){
        
        const postSchema = schema.create({      
            name: schema.string(),
            type : schema.enum(Object.values(type)),
            venue_id : schema.number()
        })

        const data = await request.validate({      
            schema: postSchema,      
            cacheKey: request.url(),    
        })

        // await Database
        //     .from('fields')
        //     .where('id', params.id)
        //     .update(data)

        let field = await Field.findBy('id', params.id)
        field = field == null ? new Field() : field
        field.name = data.name
        field.type = data.type
        field.venue_id = data.venue_id

        await field.save();
        return response.status(200).json({message: "Update success", data: field.serialize()});
    }
}
