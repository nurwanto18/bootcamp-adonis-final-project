import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class BookingVenues extends BaseSchema {
  protected tableName = 'booking_user'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('booking_id').unsigned().references('id').inTable('bookings').onDelete('CASCADE')
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
