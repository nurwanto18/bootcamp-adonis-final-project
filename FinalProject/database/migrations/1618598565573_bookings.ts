import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Bookings extends BaseSchema {
  protected tableName = 'bookings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('user_id').unsigned().references('id').inTable('users').onDelete('CASCADE')
      table.integer('field_id').unsigned().references('id').inTable('fields').onDelete('CASCADE')
      table.timestamp('play_date_start', {useTz: true}).defaultTo(this.now())
      table.timestamp('play_date_end', {useTz: true}).defaultTo(this.now())
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
