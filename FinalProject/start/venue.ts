/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'


Route.group(() => {
    Route.get('/', 'VenuesController.getAll')
    Route.get('/:id', 'VenuesController.getById')
    Route.delete('/:id', 'VenuesController.deleteById')
    Route.post('/', 'VenuesController.create')
    Route.put('/:id', 'VenuesController.updateById')

    Route.post('/validation', 'VenuesController.validation')
      
    Route.post('/booking','VenuesController.booking')

    Route.get('/join/:booking_id','VenuesController.join')

    Route.get('health', async ({ response }) => {
        const report = await HealthCheck.getReport()
        
        return report.healthy
          ? response.ok(report)
          : response.badRequest(report)
      })
}).prefix('/venue').middleware('auth')



